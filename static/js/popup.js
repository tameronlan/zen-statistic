"use strict";

var zenStat = window.zenStat || {};

(function(zenStat){
    let _q = zenStat.q = {
        byId(selector){
            return document.getElementById(selector);
        },
        byClass(selector){
            return document.getElementsByClassName(selector);
        },
        one(selector){
            return document.querySelector(selector);
        },
        all(selector){
            return document.querySelectorAll(selector);
        }
    };

    let _h = zenStat.h = {
        checkTabUrl: (opts = {}) => {
            let queryInfo = {
                active: true,
                currentWindow: true
            };

            chrome.tabs.query(queryInfo, (tabs) => {
                let tab = tabs[0];
                let url = tab.url;

                console.log(url);

                if ( opts.onSuccess && _h.isDzenUrl(url) ) opts.onSuccess(url);
                if ( opts.onFail && !_h.isDzenUrl(url) ) opts.onFail(url);
            });
        },
        isDzenUrl: (url) => {
            let urlRegex = /^https?:\/\/(?:[^./?#]+\.)?zen\.yandex\.ru\/media\/id/;

            return urlRegex.test(url);
        },
        getOffset: (elem) => {
            let top = 0;
            let left = 0;

            while(elem) {
                top = top + parseFloat(elem.offsetTop)
                left = left + parseFloat(elem.offsetLeft)
                elem = elem.offsetParent
            }

            return {top: Math.round(top), left: Math.round(left)}
        }
    };

    const STATE_PRELOADING      = '_preloading';
    const STATE_NOT_DZEN        = '_not_dzen';
    const STATE_NO_POSTS        = '_no_posts';
    const STATE_LOADING         = '_loading';
    const STATE_READY           = '_ready';

    zenStat.preLoad = () => {
        console.log('preload exec:');

        zenStat.showState(STATE_PRELOADING);

        _h.checkTabUrl({
            onSuccess: (url) => {
                let data = {url};

                console.log('listen onload:');

                zenStat.showState(STATE_LOADING);

                if ( window.loaded ){
                    zenStat.init(data);
                } else {
                    document.addEventListener('DOMContentLoaded', zenStat.init);
                }
            },
            onFail: (url) => {
                console.log('not our page:');

                zenStat.showState(STATE_NOT_DZEN);

                zenStat.showWrongPage(data);
            }
        });
    };

    zenStat.init = () => {
        console.log('onload exec:');

        zenStat.inited = true;

        zenStat.getData();

        _q.one(".js--get-data").addEventListener('click', zenStat.getData);
    };

    zenStat.setFlagloaded = () => {
        window.loaded = 1;
    };

    zenStat.showState = (state) => {
        let states = _q.all('.zenstat-state');
        let stateCurrent = _q.one('.zenstat-state.' + state);

        console.log(state);

        states.forEach( stateNode => {
            stateNode.style.display = 'none';
        });

        if ( stateCurrent ){
            stateCurrent.style.display = 'block';
        }
    };

    zenStat.getData = () => {
        if ( !zenStat.inited ){
            return;
        }

        zenStat.codeExecuter(zenStat.calcStatFromDOM, (result) => {
            let data = result;
            let countAllViewesHolder = _q.byId('zenstat-result_item_count_all_viewes');
            let countAllClicksHolder = _q.byId('zenstat-result_item_count_all_clicks');
            let countAllPostsHolder = _q.byId('zenstat-result_item_count_all_posts');
            let avgCtrHolder = _q.byId('zenstat-result_item_ctr_avg');
            let bestCtrHolder = _q.byId('zenstat-result_item_ctr_best');
            let badCtrHolder = _q.byId('zenstat-result_item_ctr_bad');
            let bestPostLink = _q.byId('zenstat-result_item_best_post');
            let badPostLink = _q.byId('zenstat-result_item_bad_post');

            if ( typeof data.avgCtrAll === 'undefined' ){
                zenStat.showState(STATE_LOADING);

                return;
            }

            if ( !data.countAllPosts ){
                console.log('render emoty page')

                zenStat.showState(STATE_NO_POSTS);

                return;
            }

            if ( countAllViewesHolder && data.countAllViews ){
                countAllViewesHolder.innerHTML = data.countAllViews;
            }

            if ( countAllClicksHolder && data.countAllClicks ){
                countAllClicksHolder.innerHTML = data.countAllClicks;
            }

            if ( countAllPostsHolder && data.countAllPosts ){
                countAllPostsHolder.innerHTML = data.countAllPosts;
            }

            if ( avgCtrHolder && data.avgCtrAll ){
                avgCtrHolder.innerHTML = +data.avgCtrAll.toFixed(2);
            }

            if ( bestCtrHolder ){
                bestCtrHolder.innerHTML = +data.bestCtr.toFixed(2);
            }

            if ( badCtrHolder ){
                badCtrHolder.innerHTML = +data.badCtr.toFixed(2);
            }

            if ( bestPostLink ){
                bestPostLink.addEventListener('click', ()=>{
                    zenStat.handlerShowPost(data.bestPostIndex);
                });
            }

            if ( badPostLink ){
                badPostLink.addEventListener('click', ()=>{
                    zenStat.handlerShowPost(data.badPostIndex);
                });
            }

            zenStat.showState(STATE_READY);
        });
    };

    zenStat.handlerShowPost = (index) => {
        var script = 'function(){ window.postForShow = "' + index + '" }';

        zenStat.codeExecuter(script, () => {
            zenStat.codeExecuter(zenStat.highlightPost, (result) => {});
        })
    };


    zenStat.setPostViewIndex = (index) => {
        window.postForView = index;
    };

    zenStat.highlightPost = () => {
        var posts = document.querySelectorAll('.publication-card-item')

        var getOffset = (elem) => {
            let top = 0;
            let left = 0;

            while(elem) {
                top = top + parseFloat(elem.offsetTop)
                left = left + parseFloat(elem.offsetLeft)
                elem = elem.offsetParent
            }

            return {top: Math.round(top), left: Math.round(left)}
        }

        posts.forEach((post, index) => {
            post.style.border = 'none';
        });

        if ( window.postForShow && posts[window.postForShow] ){
            let currentPost = posts[window.postForShow];
            let offset = getOffset(currentPost);

            currentPost.style.transition = 'all .3s linear';
            currentPost.style.border = '3px solid #199bc8';

            window.scrollTo(0, offset.top);
        }

        return true;
    };

    zenStat.calcStatFromDOM = () => {
        var posts = document.querySelectorAll('.publication-card-item')
        var countAllViews = 0;
        var countAllClicks = 0;
        var cnts;
        var bestCtr = 0;
        var bestPostIndex;
        var badCtr = false;
        var badPostIndex;

        posts.forEach((post, index) => {
            let currentViewes;
            let currentClicks;

            cnts = post.querySelectorAll('.publication-card-item__stat-main-count');

            if ( cnts[0].innerHTML.indexOf(' ') !== -1 ){
                currentViewes = ( + ( cnts[0].innerHTML.split(' ')[0] ) ) * 1000 ;
            } else {
                currentViewes = +cnts[0].innerHTML
            }

            if ( cnts[1].innerHTML.indexOf(' ') !== -1 ){
                currentClicks = ( + ( cnts[1].innerHTML.split(' ')[0] ) ) * 1000 ;
            } else {
                currentClicks = +cnts[1].innerHTML
            }

            countAllViews += currentViewes;
            countAllClicks += currentClicks;

            if ( currentViewes > 20 && bestCtr < (currentClicks / currentViewes) ) {
                bestCtr = currentClicks / currentViewes;

                bestPostIndex = index;
            }

            if ( ( !badCtr || badCtr > (currentClicks / currentViewes) ) && currentViewes > 0 ){
                badCtr = currentViewes === 0 ? 0 : currentClicks / currentViewes;

                badPostIndex = index;
            }
        });

        var result = {
            countAllViews: countAllViews,
            countAllClicks: countAllClicks,
            countAllPosts: posts.length,
            avgCtrAll: (countAllClicks * 100 / countAllViews),
            bestCtr: bestCtr * 100,
            badCtr: badCtr * 100,
            badPostIndex: badPostIndex,
            bestPostIndex: bestPostIndex,
        };

        return result;
    };

    zenStat.codeExecuter = (code, callback) => {
        chrome.tabs.executeScript({
            code: '(' + code + ')();'
        }, (results) => {
            console.log(results[0]);

            if ( callback ){
                callback(results[0]);
            }
        });
    };
})(window.zenStat);

document.addEventListener('DOMContentLoaded', zenStat.setFlagloaded);

zenStat.preLoad();